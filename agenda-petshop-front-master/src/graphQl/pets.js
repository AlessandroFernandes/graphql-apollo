import gql from 'graphql-tag';

export const LISTAR_PETS = gql`
            query {
                Pets {
                    id
                    nome
                    tipo
                    observacoes
                    dono {
                        id
                        nome
                    }
                }
            }
`;

export const ADICIONAR_PETS = gql`
    mutation AdicionarPet($nome: String!, $donoId: Int!, $tipo: String!, $observacoes: String)
    {
        AdicionarPet(nome: $nome, donoId: $donoId, tipo: $tipo, observacoes: $observacoes)
        {
            nome
        }
    }
`;