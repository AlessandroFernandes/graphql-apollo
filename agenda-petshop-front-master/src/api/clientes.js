import { api } from './config'

const API = 'http://localhost:4000/';

let headers = query => (
  {
    method: 'post',
    headers: {
      'Content-Type' : 'application/json'
    },
    body: JSON.stringify({
      query
    })
  }
)
const listarClientes = () => 
  fetch(API, headers('{Clientes {id nome cpf}}'))
  .then(data => data.json())
  .then(data => data.data.Clientes)
  .catch(() => "Erro na API")

const buscarClientePorId = id =>
  fetch(API, headers(`{Cliente(id: ${id})
                       { id nome cpf}
  }`))
  .then(data => data.json())
  .then(data => data.data.Cliente)
  .catch(() => 'Erro na API')
 
const adicionarCliente = cliente => 
fetch(API, headers(`mutation {
                      AdicionarCliente(nome: "${cliente.nome}", cpf: "${cliente.cpf}")
                      { id nome cpf}
 }`))
.then(data => data.json())
.then(data => data.data)
.catch(() => "Erro na API")

const alterarCliente = (id, cliente) =>
fetch(API, headers(`mutation {
                     AtualizarCliente(id: ${Number(id)}, nome: "${cliente.nome}", cpf: "${cliente.cpf}")
                     { id nome cpf }
}`))
.then(data => data.json())
.then(data => data.data.AtualizarCliente)
.catch(() => "Erro na API")

const removerCliente = id => 
  fetch(API, headers(`mutation {
                        DeletarCliente(id: ${Number(id)})
  }`))

export default {
  listarClientes,

  buscarClientePorId,
  adicionarCliente,
  alterarCliente,
  removerCliente
}