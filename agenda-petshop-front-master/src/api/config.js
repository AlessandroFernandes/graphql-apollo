import axios from 'axios'
import { ApolloClient, InMemoryCache } from '@apollo/client';

export const api = axios.create({
  baseURL: 'http://localhost:4000',
  timeout: 10000,
  headers: {
    'content-type': 'application/json',
  }
})

export const apolloClient = new ApolloClient({
  uri: "http://localhost:4000/",
  cache: new InMemoryCache()
})